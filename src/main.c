#include "mem.h"

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>

#define INITIAL_HEAP_SIZE 8192
#define TEST1_ARRAY_BASE_SIZE 2000

#define GREEN_COLOR "\033[0;32m"
#define RED_COLOR "\033[0;31m"
#define RESET_COLOR "\033[0m"

size_t TEST_NUMBER = 1;

void test_single_array(const size_t array_size, const bool print_array, const void* heap) {
    fprintf(stdout, "Test №%zu:\n", TEST_NUMBER++);

    uint32_t* array = (uint32_t*) _malloc(sizeof(uint32_t) * array_size);

    if (!array) {
        fprintf(stderr, RED_COLOR "Test failed" RESET_COLOR "\n");
        fprintf(stderr, "Explanation: can't allocate memory for an array of uint32_t and size: %zu\n", array_size);
        return;
    }

    fprintf(stdout, "After _malloc array of uint32_t. Size: %zu\n", array_size);
    debug_heap(stdout, heap);

    if (print_array) {
        fprintf(stdout, "Filling array:\n");
    }

    for (size_t i = 0; i < array_size; i++) {
        array[i] = i + 1;

        if (print_array) {
            fprintf(stdout, "%p: %" PRId32 "\n", (void*)(array + i), array[i]);
        }
    }

    _free(array);

    fprintf(stdout, "After _free the array");
    debug_heap(stdout, heap);

    fprintf(stdout, GREEN_COLOR "Test passed" RESET_COLOR "\n");
}

void test_many_blocks_1(const void* heap) {
    fprintf(stdout, "Test №%zu:\n", TEST_NUMBER++);

    void* first = _malloc(1000);
    if (!first) {
        fprintf(stderr, RED_COLOR "Test failed" RESET_COLOR "\n");
        fprintf(stderr, "Explanation: can't allocate memory block for size: %d\n", 1000);
        return;
    }

    void* second = _malloc(2000);
    if (!second) {
        fprintf(stderr, RED_COLOR "Test failed" RESET_COLOR "\n");
        fprintf(stderr, "Explanation: can't allocate memory block for size: %d\n", 2000);
        return;
    }

    void* third = _malloc(1000);
    if (!third) {
        fprintf(stderr, RED_COLOR "Test failed" RESET_COLOR "\n");
        fprintf(stderr, "Explanation: can't allocate memory block for size: %d\n", 1000);
        return;
    }

    void* fourth = _malloc(2000);
    if (!fourth) {
        fprintf(stderr, RED_COLOR "Test failed" RESET_COLOR "\n");
        fprintf(stderr, "Explanation: can't allocate memory block for size: %d\n", 2000);
        return;
    }

    void* fifth = _malloc(1000);
    if (!fifth) {
        fprintf(stderr, RED_COLOR "Test failed" RESET_COLOR "\n");
        fprintf(stderr, "Explanation: can't allocate memory block for size: %d\n", 1000);
        return;
    }

    fprintf(stdout, "After _malloc 5 blocks of sizes: 1000 2000 1000 2000 1000\n");
    debug_heap(stdout, heap);

    _free(third);

    fprintf(stdout, "After _free third block\n");
    debug_heap(stdout, heap);

    _free(first);
    _free(second);
    _free(fourth);
    _free(fifth);

    fprintf(stdout, GREEN_COLOR "Test passed" RESET_COLOR "\n");
}

void test_many_blocks_2(const void* heap) {
    fprintf(stdout, "Test №%zu:\n", TEST_NUMBER++);

    void* first = _malloc(1000);
    if (!first) {
        fprintf(stderr, RED_COLOR "Test failed" RESET_COLOR "\n");
        fprintf(stderr, "Explanation: can't allocate memory block for size: %d\n", 1000);
        return;
    }

    void* second = _malloc(2000);
    if (!second) {
        fprintf(stderr, RED_COLOR "Test failed" RESET_COLOR "\n");
        fprintf(stderr, "Explanation: can't allocate memory block for size: %d\n", 2000);
        return;
    }

    void* third = _malloc(1000);
    if (!third) {
        fprintf(stderr, RED_COLOR "Test failed" RESET_COLOR "\n");
        fprintf(stderr, "Explanation: can't allocate memory block for size: %d\n", 1000);
        return;
    }

    void* fourth = _malloc(2000);
    if (!fourth) {
        fprintf(stderr, RED_COLOR "Test failed" RESET_COLOR "\n");
        fprintf(stderr, "Explanation: can't allocate memory block for size: %d\n", 2000);
        return;
    }

    void* fifth = _malloc(1000);
    if (!fifth) {
        fprintf(stderr, RED_COLOR "Test failed" RESET_COLOR "\n");
        fprintf(stderr, "Explanation: can't allocate memory block for size: %d\n", 1000);
        return;
    }

    fprintf(stdout, "After _malloc 5 blocks of sizes: 1000 2000 1000 2000 1000\n");
    debug_heap(stdout, heap);

    _free(third);

    fprintf(stdout, "After _free third block\n");
    debug_heap(stdout, heap);

    _free(fourth);

    fprintf(stdout, "After _free third and fourth blocks\n");
    debug_heap(stdout, heap);

    _free(second);

    fprintf(stdout, "After _free third, fourth and second blocks\n");
    debug_heap(stdout, heap);

    _free(first);
    _free(fifth);

    fprintf(stdout, GREEN_COLOR "Test passed" RESET_COLOR "\n");
}

void test_min_block_capacity(const void* heap) {
    fprintf(stdout, "Test №%zu:\n", TEST_NUMBER++);

    uint8_t* ptr = _malloc(sizeof(uint8_t));
    fprintf(stdout, "After _malloc single byte\n");
    debug_heap(stdout, heap);

    _free(ptr);

    fprintf(stdout, GREEN_COLOR "Test passed" RESET_COLOR "\n");
}

//for test only
static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

void test_block_sequence(const void* heap) {
    fprintf(stdout, "Test №%zu:\n", TEST_NUMBER++);

    uint8_t* first_array_ptr = (uint8_t*) _malloc(sizeof(uint8_t) * 8000);
    fprintf(stdout, "After _malloc first array of size 8000\n");
    debug_heap(stdout, heap);

    void* page_ptr = map_pages(first_array_ptr + (8192 - 17), 4096, MAP_FIXED_NOREPLACE);

    uint8_t* second_array_ptr = (uint8_t*) _malloc(sizeof(uint8_t) * 2000);
    fprintf(stdout, "After _malloc second array of size 2000\n");
    debug_heap(stdout, heap);

    _free(second_array_ptr);
    _free(first_array_ptr);
    munmap(page_ptr, 4096);

    fprintf(stdout, GREEN_COLOR "Test passed" RESET_COLOR "\n");
}

int main(int argc, char** argv) {
    void* heap = heap_init(INITIAL_HEAP_SIZE);
    debug_heap(stdout, heap);

    if (argc == 1) {
        test_min_block_capacity(heap);
        printf("\n\n\n");

        test_single_array(TEST1_ARRAY_BASE_SIZE, false, heap);
        printf("\n\n\n");

        test_many_blocks_1(heap);
        printf("\n\n\n");

        test_many_blocks_2(heap);
        printf("\n\n\n");

        test_single_array(10 * TEST1_ARRAY_BASE_SIZE, false, heap);
        printf("\n\n\n");
    } else if (argc == 2 && strcmp(argv[1], "seq") == 0) {
        test_block_sequence(heap);
        printf("\n\n\n");
    } else {
        fprintf(stderr, "You can pass only single parameter 'seq'\n");
    }

    return 0;
}
