#include <stdarg.h>

#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query, bool strict) {
    block_size region_size = (block_size) {.bytes = region_actual_size(query)};

    void *result_addr;
    if (strict) {
        result_addr = map_pages(addr, region_size.bytes, MAP_FIXED_NOREPLACE);
    } else {
        result_addr = map_pages(addr, region_size.bytes, 0);
    }

    if (result_addr == MAP_FAILED) {
        return REGION_INVALID;
    }

    block_init(result_addr, region_size, NULL);
    return (struct region) {
            .addr = result_addr,
            .size = region_size.bytes,
            .extends = false
    };
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial, true);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (!block) {
        return false;
    }

    if (query <= BLOCK_MIN_CAPACITY) {
        query = BLOCK_MIN_CAPACITY;
    }

    if (!block_splittable(block, query)) {
        return false;
    }

    if (!block_is_big_enough(query, block)) {
        return false;
    }

    void *new_block_addr = block->contents + query;
    block_init(new_block_addr, (block_size) {.bytes = block->capacity.bytes - query}, block->next);
    block->capacity = (block_capacity) {.bytes = query};
    block->next = new_block_addr;

    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    if (!block->next) {
        return false;
    }

    if (!mergeable(block, block->next)) {
        return false;
    }

    block->capacity = (block_capacity) {.bytes = block->capacity.bytes +
                                                 size_from_capacity(block->next->capacity).bytes};
    block->next = block->next->next;
    return true;
}

static void merge_all_possible_blocks_starts_at(struct block_header* block) {
    struct block_header *curr = block;

    while (curr) {
        while (try_merge_with_next(curr)) {}
        curr = curr->next;
    }
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    while (true) {
        if (block->is_free && block_is_big_enough(sz, block)) {
            return (struct block_search_result) {
                    .type = BSR_FOUND_GOOD_BLOCK,
                    .block = block
            };
        }

        if (!block->next) {
            return (struct block_search_result) {
                    .type = BSR_REACHED_END_NOT_FOUND,
                    .block = block
            };
        }

        block = block->next;
    }
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    merge_all_possible_blocks_starts_at(block);
    return find_good_or_last(block, query);
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    struct region new_region = alloc_region(block_after(last), query, 1);

    if (region_is_invalid(&new_region)) {
        new_region = alloc_region(block_after(last), query, 0);

        if (region_is_invalid(&new_region)) {
            return NULL;
        }
    }

    struct block_header *new_block_header = (struct block_header *) new_region.addr;
    last->next = new_block_header;

    if (try_merge_with_next(last)) {
        return last;
    }

    return new_block_header;
}

static void use_block(size_t query, struct block_header *block) {
    split_if_too_big(block, query);
    block->is_free = false;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    struct block_search_result bsr = try_memalloc_existing(query, heap_start);

    if (bsr.type == BSR_FOUND_GOOD_BLOCK) {
        use_block(query, bsr.block);
        return bsr.block;
    }

    if (bsr.type == BSR_REACHED_END_NOT_FOUND) {
        struct block_header *last_block = bsr.block;
        struct block_header *new_region_block = grow_heap(last_block, query);

        if (!new_region_block) {
            return NULL;
        }

        use_block(query, new_region_block);
        return new_region_block;
    }

    return NULL;
}

void *_malloc(size_t query) {
    if (query == 0) {
        return NULL;
    }

    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) {
        return addr->contents;
    } else {
        return NULL;
    }
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    merge_all_possible_blocks_starts_at(header);
}
